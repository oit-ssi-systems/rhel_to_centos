#!/bin/bash -x 

hostnamectl

yum-config-manager --disable patchmonkey-rhel-7-server-extras-rpms.repo 
yum-config-manager --disable patchmonkey-rhel-7-server-optional-rpms.repo 
yum-config-manager --disable patchmonkey-rhel-7-server-rpms.repo 
yum-config-manager --disable patchmonkey-rhel-7-server-supplementary-rpms.repo 
yum-config-manager --disable patchmonkey-rhel-server-rhscl-7-rpms.repo

cd /etc/yum.repos.d/ 
mv patchmonkey-rhel-7-server-extras-rpms.repo /tmp/
mv patchmonkey-rhel-7-server-optional-rpms.repo /tmp/
mv patchmonkey-rhel-7-server-rpms.repo /tmp/
mv patchmonkey-rhel-7-server-supplementary-rpms.repo /tmp/
mv patchmonkey-rhel-server-rhscl-7-rpms.repo /tmp/

cat <<\EOF >> centos-7-os.repo
[base]
name=centos-7-os
baseurl=http://mirror.linux.duke.edu/centos/7/os/x86_64/
gpgcheck=0
enabled=1
EOF

cat <<\EOF >> centos-7-rt.repo
[extras]
name=centos-7-rt
baseurl=http://mirror.linux.duke.edu/centos/7/rt/x86_64/
gpgcheck=0
enabled=1
EOF

cat <<\EOF >> centos-7-sclo.repo
[updates]
name=centos-7-sclo
baseurl=http://mirror.linux.duke.edu/centos/7/sclo/x86_64/sclo/
gpgcheck=0
enabled=1
EOF

yum-config-manager --enable repository extras
yum-config-manager --enable repository base
yum-config-manager --enable repository updates

yum remove -y rhnlib redhat-support-tool redhat-support-lib-python
rpm -qa| egrep "rhn|redhat" | xargs rpm -e --nodeps

rm -rf /usr/share/redhat-release/
rm -rf /usr/share/doc/redhat-release/

yum install centos-logos centos-release -y

# Clean database & upgrade all packages
yum clean all
yum install -y deltarpm yum-presto
yum upgrade -y
yum -y einstall shim kernel 
grub2-mkconfig -o /boot/grub2/grub.cfg
efibootmgr -c -d /dev/sda -p 1 -l '\EFI\centos\shim.efi' -L 'CentOS'
cp /boot/grub2/grub.cfg /boot/efi/EFI/centos/

efibootmgr  -v


if [ -f /boot/efi/EFI/centos/grub.cfg -a -f /boot/efi/EFI/centos/shimx64.efi -a -f /boot/efi/EFI/centos/grubx64.efi -a -f /boot/efi/EFI/centos/mmx64.efi -a -f /boot/efi/EFI/centos/shimx64-centos.efi ]; then
    echo "Conversion to CENTOS is Complete!"
else
    echo "Missing Files in /boot/efi/EFI/centos/  DON'T REBOOT BEFORE FIXING THE ISSUE!!!!"
    tree /boot/efi
fi

