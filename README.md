**RHEL 8 conversion to CentOS 8**

After running the script if you don't see any errors, make sure the following exists before rebooting the server

% ls /boot/efi/EFI/centos
* BOOTX64.CSV
* fonts
* grub.cfg
* grubenv
* grubx64.efi
* mmx64.efi
* shimx64-centos.efi
* shimx64.efi


If any of these files are missing don't reboot! Follow the steps:
```
grub2-mkconfig -o /boot/efi/EFI/centos/grub.cfg
wget http://mirror.centos.org/centos/8/BaseOS/x86_64/os/Packages/shim-x64-15-8.el8.x86_64.rpm
yum localinstall shim-x64-15-8.el8.x86_64.rpm
```

Print EFI boot manager
efibootmgr -v
Centos should be listed last. Sample below

Boot0007* CentOS  HD(1,GPT)

if you don't see centos listed run the following:
```
efibootmgr -c -d /dev/sda -p 1 -l '\EFI\centos\shim.efi' -L 'CentOS'
efibootmgr -v
```
