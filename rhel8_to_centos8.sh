#!/bin/bash -x

hostnamectl
echo 'checking rhel version'

elversion=$(lsb_release -s -r | cut -d '.' -f 1)
if [[ $elversion !=  8 ]]
then
        echo 'Server is not running Red Hat 8'
        exit 1
fi


cd /etc/pki/rpm-gpg/
wget https://www.centos.org/keys/RPM-GPG-KEY-CentOS-Official
cp RPM-GPG-KEY-CentOS-Official RPM-GPG-KEY-centosofficial
gpg --quiet --with-fingerprint RPM-GPG-KEY-centosofficial

subscription-manager config --rhsm.manage_repos=0

cd /etc/yum.repos.d
mv patchmonkey-rhel-8-for-x86_64-appstream-rpms.repo /tmp/
mv patchmonkey-rhel-8-for-x86_64-baseos-rpms.repo /tmp/
mv patchmonkey-rhel-8-for-x86_64-supplementary-rpms.repo /tmp/

echo "Creating AppStream Repo"

cat <<\EOF >> CentOS-AppStream.repo
[AppStream]
name=CentOS-$releasever - AppStream
mirrorlist=http://mirrorlist.centos.org/?release=$releasever&arch=$basearch&repo=AppStream&infra=$infra
#baseurl=http://mirror.centos.org/$contentdir/$releasever/AppStream/$basearch/os/
gpgcheck=1
enabled=1
gpgkey=file:///etc/pki/rpm-gpg/RPM-GPG-KEY-centosofficial
EOF

echo "Creating BaseOS Repo"

cat <<\EOF >> CentOS-Base.repo
[BaseOS]
name=CentOS-$releasever - Base
mirrorlist=http://mirrorlist.centos.org/?release=$releasever&arch=$basearch&repo=BaseOS&infra=$infra
#baseurl=http://mirror.centos.org/$contentdir/$releasever/BaseOS/$basearch/os/
gpgcheck=1
enabled=1
gpgkey=file:///etc/pki/rpm-gpg/RPM-GPG-KEY-centosofficial
EOF

echo "Creating CentOS Extras Repo"

cat <<\EOF >> CentOS-Extras.repo
[extras]
name=CentOS-$releasever - Extras
mirrorlist=http://mirrorlist.centos.org/?release=$releasever&arch=$basearch&repo=extras&infra=$infra
#baseurl=http://mirror.centos.org/$contentdir/$releasever/extras/$basearch/os/
gpgcheck=1
enabled=1
gpgkey=file:///etc/pki/rpm-gpg/RPM-GPG-KEY-centosofficial
EOF

cat <<\EOF >> CentOS-PowerTools.repo
[PowerTools]
name=CentOS-$releasever - PowerTools
mirrorlist=http://mirrorlist.centos.org/?release=$releasever&arch=$basearch&repo=PowerTools&infra=$infra
#baseurl=http://mirror.centos.org/$contentdir/$releasever/PowerTools/$basearch/os/
gpgcheck=1
enabled=1
gpgkey=file:///etc/pki/rpm-gpg/RPM-GPG-KEY-centosofficial
EOF

yum clean all
rpm -qa | sort | sed 's/-[0-9].*[0-9]*//g' > /tmp/rpmslist.txt
cat /tmp/rpmslist.txt | xargs yum reinstall -y

echo "Number of Packages Still under Redhat"
rpm -qai | grep -c 'Vendor.*Red'

yum reinstall yum -y
yum erase dnf-plugin-subscription-manager
grub2-mkconfig -o /boot/efi/EFI/centos/grub.cfg
yum -y remove redhat-release-eula
rpm -e --nodeps redhat-release
rm -rf /usr/share/redhat-release
rm -rf /usr/share/doc/redhat-release
yum install -y centos-release --releasever=8
yum -y install tree
tree /boot/efi

echo "Display UFI Boot Manager"
efibootmgr -v

echo "Update GRUB Boot loader Configuration"
efibootmgr -c -d /dev/sda -p 1 -l '\EFI\centos\shim.efi' -L 'CentOS'
efibootmgr -v

ll /boot/efi/EFI/centos/grub.cfg

wget http://mirror.centos.org/centos/8/BaseOS/x86_64/os/Packages/shim-x64-15-8.el8.x86_64.rpm
yum localinstall -y shim-x64-15-8.el8.x86_64.rpm --releasever=8

cp /boot/efi/EFI/redhat/grubx64.efi /boot/efi/EFI/centos/grubx64.efi


if [ -f /boot/efi/EFI/centos/grub.cfg -a -f /boot/efi/EFI/centos/shimx64.efi -a -f /boot/efi/EFI/centos/grubx64.efi -a -f /boot/efi/EFI/centos/mmx64.efi -a -f /boot/efi/EFI/centos/shimx64-centos.efi ]; then
    echo "Conversion to CENTOS is Complete!"
else
    echo "Missing Files in /boot/efi/EFI/centos/  DON'T REBOOT BEFORE FIXING THE ISSUE!!!!"
    tree /boot/efi
fi
~
